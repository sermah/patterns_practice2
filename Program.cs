﻿

/**
Махинов Сергей БСБО-10-21
Паттерны Компоновщик* и Шаблонный метод
*/

namespace Practice
{
    public static class Practice
    {
        public static void Main(string[] args)
        {
            Renderer renderer = new Renderer();

            renderer.Init();
            renderer.Draw();
        }
    }
}