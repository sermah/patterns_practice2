namespace Practice
{
    /* Component */
    interface Shape
    {
        public void Draw();
        public void Move(int x, int y);
    }

    class CompositeShape : Shape
    {
        List<Shape> children = new List<Shape>();

        public void Draw()
        {
            foreach (Shape child in children)
            {
                child.Draw();
            }
        }

        public void Move(int x, int y)
        {
            foreach (Shape child in children)
            {
                child.Move(x, y);
            }
        }

        public void AddChild(Shape child)
        {
            children.Add(child);
        }

        public void RemoveChild(Shape child)
        {
            children.Remove(child);
        }
    }

    abstract class ShapeImpl : Shape
    {
        protected int ox = 0, oy = 0;

        public ShapeImpl(int x, int y)
        {
            this.ox = x;
            this.oy = y;
        }

        public void Draw()
        {
            OnFill();
            OnOutline();
        }

        public void Move(int dx, int dy)
        {
            ox += dx;
            oy += dy;
        }

        virtual protected void OnFill() { }
        virtual protected void OnOutline() { }
    }

    class Dot : ShapeImpl
    {
        char color = 'O';

        public Dot(int x, int y) : base(x, y) { }

        public Dot(int x, int y, char color) : base(x, y)
        {
            this.color = color;
        }

        override protected void OnFill()
        {
            Canvas.Draw(ox, oy, color);
        }
    }

    class Rect : ShapeImpl
    {
        protected char outline = '#';
        protected char fill = '.';

        protected int width;
        protected int height;

        public Rect(int x1, int y1, int x2, int y2) : base(x1, y1)
        {
            ox = Math.Min(x1, x2);
            width = Math.Max(x1, x2) - ox + 1;
            oy = Math.Min(y1, y2);
            height = Math.Max(y1, y2) - oy + 1;
        }

        public Rect(int x1, int y1, int x2, int y2, char color) : this(x1, y1, x2, y2)
        {
            this.outline = color;
            this.fill = color;
        }

        public Rect(int x1, int y1, int x2, int y2, char outline, char fill) : this(x1, y1, x2, y2, outline)
        {
            this.fill = fill;
        }

        override protected void OnOutline()
        {
            for (int ix = ox; ix < ox + width; ix++)
            {
                Canvas.Draw(ix, oy, outline);
                Canvas.Draw(ix, oy + height - 1, outline);
            }
            for (int iy = oy; iy < oy + height; iy++)
            {
                Canvas.Draw(ox, iy, outline);
                Canvas.Draw(ox + width - 1, iy, outline);
            }
        }
        override protected void OnFill()
        {
            for (int ix = ox; ix < ox + width; ix++)
            {
                for (int iy = oy; iy < oy + height; iy++)
                {
                    Canvas.Draw(ix, iy, fill);
                }
            }
        }
    }

    class HollowRect : Rect
    {
        public HollowRect(int x1, int y1, int x2, int y2) : base(x1, y1, x2, y2) {}
        public HollowRect(int x1, int y1, int x2, int y2, char outline) : base(x1, y1, x2, y2, outline) {}

        override protected void OnFill() {}
    }

    class Ellipse : Rect
    {
        public Ellipse(int x1, int y1, int x2, int y2) : base(x1, y1, x2, y2) {
            ox = (x1 + x2) / 2;
            oy = (y1 + y2) / 2;
        }
        public Ellipse(int x1, int y1, int x2, int y2, char color) : base(x1, y1, x2, y2, color) {
            ox = (x1 + x2) / 2;
            oy = (y1 + y2) / 2;
        }
        public Ellipse(int x1, int y1, int x2, int y2, char outline, char fill) : base(x1, y1, x2, y2, outline, fill) {
            ox = (x1 + x2) / 2;
            oy = (y1 + y2) / 2;
        }

        override protected void OnOutline()
        {
            double dx, dy, d1, d2, x, y;
            double rx = width / 2f - 0.01;
            double ry = height / 2f - 0.01;
            x = 0;
            y = ry;

            // Initial decision parameter of region 1
            d1 = (ry * ry) - (rx * rx * ry) +
                            (0.25f * rx * rx);
            dx = 2 * ry * ry * x;
            dy = 2 * rx * rx * y;

            // For region 1
            while (dx < dy)
            {

                // Print points based on 4-way symmetry
                Canvas.Draw((int) x + ox, (int) y + oy, outline);
                Canvas.Draw((int) x + ox, (int) -y + oy, outline);
                Canvas.Draw((int) -x + ox, (int) y + oy, outline);
                Canvas.Draw((int) -x + ox, (int) -y + oy, outline);

                // Checking and updating value of
                // decision parameter based on algorithm
                if (d1 < 0)
                {
                    x++;
                    dx = dx + (2 * ry * ry);
                    d1 = d1 + dx + (ry * ry);
                }
                else
                {
                    x++;
                    y--;
                    dx = dx + (2 * ry * ry);
                    dy = dy - (2 * rx * rx);
                    d1 = d1 + dx - dy + (ry * ry);
                }
            }

            // Decision parameter of region 2
            d2 = ((ry * ry) * ((x + 0.5f) * (x + 0.5f)))
                + ((rx * rx) * ((y - 1) * (y - 1)))
                - (rx * rx * ry * ry);

            // Plotting points of region 2
            while (y >= 0)
            {

                // printing points based on 4-way symmetry
                Canvas.Draw((int) x + ox, (int) y + oy, outline);
                Canvas.Draw((int) x + ox, (int) -y + oy, outline);
                Canvas.Draw((int) -x + ox, (int) y + oy, outline);
                Canvas.Draw((int) -x + ox, (int) -y + oy, outline);

                // Checking and updating parameter
                // value based on algorithm
                if (d2 > 0)
                {
                    y--;
                    dy = dy - (2 * rx * rx);
                    d2 = d2 + (rx * rx) - dy;
                }
                else
                {
                    y--;
                    x++;
                    dx = dx + (2 * ry * ry);
                    dy = dy - (2 * rx * rx);
                    d2 = d2 + dx - dy + (rx * rx);
                }
            }
        }
        override protected void OnFill()
        {
            int w2 = width / 2;
            int h2 = height / 2;

            int w2sq = w2 * w2;
            int h2sq = h2 * h2;
            int whsq = h2sq * w2sq;

            for (int ix = -w2; ix < w2; ix++)
            {
                int ixsq = ix * ix;
                for (int iy = -h2; iy < h2; iy++)
                {
                    if (ixsq * h2sq + iy * iy * w2sq <= whsq - 1)
                        Canvas.Draw(ox + ix, oy + iy, fill);
                }
            }
        }
    }

    class HollowEllipse : Ellipse
    {
        public HollowEllipse(int x1, int y1, int x2, int y2) : base(x1, y1, x2, y2) {}
        public HollowEllipse(int x1, int y1, int x2, int y2, char outline) : base(x1, y1, x2, y2, outline) {}

        override protected void OnFill() {}
    }
}