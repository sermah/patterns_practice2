# Практическая работа №2

Реализация паттернов проектирования

Выполнил - Махинов Сергей БСБО-10-21

## Паттерны

- Компоновщик*
- Шаблонный метод

## Как работает

У нас есть класс Рисовальщик (`Renderer`), который отрисовывает различные фигуры. Он является **компоновщиком**:

- имеет композитную фигуру, у которой вызывает метод `Draw()`
- в методе `Init()` мы добавляем в него различные фигуры

У нас есть интерфейс Фигура (`Shape`), который является компонентом:

- `Draw()` - отрисовать фигуру на холсте
- `Move(x, y)` - переместить фигуру на x, y

Интерфейс Фигура реализуют следующие классы:

- `ShapeImpl` - конечная простая фигура, абстрактный класс, с **шаблонным методом** `Draw()`
    - в шаблонном методе `Draw` содержатся методы `OnFill` и `OnOutline`
- `CompositeShape` - сложная фигура, имеющая от нуля до нескольких дочерних фигур
    - помимо методов интерфейса, присутствуют методы `AddChild` и `RemoveChild`

Абстрактный класс ShapeImpl наследуют следующие классы:

- `Dot` - точка
- `Rect` - прямоугольник
    - `HollowRect` - незаполненный прямоугольник
    - `Ellipse` - эллипс
        - `HollowEllipse` - незаполненный эллипс

Для демонстрации работы программы, есть еще один класс `Canvas`:

- Это статический класс, содержит в себе 3 метода:
    - `Draw(x, y, ch)` - отрисовывает символ на 2д-массиве символов (холсте)
    - `Clear()` - очищает "холст"
    - `Render()` - отрисовывает "холст" в консоли
- Сделан для отрисовки фигур без сложных библиотек :)

```plantuml
@startuml

class Renderer
{
    -all : Shape

    +Init()
    +Draw()
}

interface Shape
{
    +Draw()
    +Move(x, y)
}

class CompositeShape
{
    -children : List<Shape>

    +AddChild(shape)
    +RemoveChild(shape)
    +Draw()
    +Move(x, y)
}

abstract ShapeImpl
{
    ox : int
    oy : int

    +Draw()
    +Move(x, y)
    OnFill()
    OnOutline()
}

class Dot
{
    ox : int
    oy : int
    color : char

    +Draw()
    +Move(x, y)
    OnFill()
    OnOutline()
}

class Rect
{
    ox : int
    oy : int
    width : int
    height : int
    outline : char
    fill : char

    +Draw()
    +Move(x, y)
    OnFill()
    OnOutline()
}

class HollowRect
{
    ox : int
    oy : int
    width : int
    height : int
    outline : char
    --fill-- : char

    +Draw()
    +Move(x, y)
    OnOutline()
    --OnFill--()
}

class Ellipse
{
    ox : int
    oy : int
    width : int
    height : int
    outline : char
    fill : char

    +Draw()
    +Move(x, y)
    OnFill()
    OnOutline()
}

class HollowEllipse
{
    ox : int
    oy : int
    width : int
    height : int
    outline : char
    --fill-- : char

    +Draw()
    +Move(x, y)
    OnOutline()
    --OnFill--()
}

Shape <|-- ShapeImpl : Реализует
Shape <|-- CompositeShape : Реализует
ShapeImpl <|-- Dot : Наследует
ShapeImpl <|-- Rect : Наследует
Rect <|-- HollowRect : Наследует
Rect <|-- Ellipse : Наследует
Ellipse <|-- HollowEllipse : Наследует

Shape "1" -* "1" Renderer : Содержит
CompositeShape "1" o-- "0..*" Shape : Содержит

@enduml
```