namespace Practice
{
    public static class Canvas
    {
        public static char[,] data = new char[80, 20];

        public static int width { get => data.GetLength(0); }
        public static int height { get => data.GetLength(1); }

        static Canvas() {
            Clear();
        }

        public static void Draw(int x, int y, char color)
        {
            if (x < 0 || x >= width || y < 0 || y >= height)
                return;

            data[x, y] = color;
        }

        public static void Render()
        {
            Console.Write('+');
            for (int x = 0; x < width; x++)
                Console.Write('-');
            Console.WriteLine('+');

            for (int y = 0; y < height; y++)
            {
                Console.Write('|');
                for (int x = 0; x < width; x++)
                {
                    Console.Write(data[x, y]);
                }
                Console.WriteLine('|');
            }

            Console.Write('+');
            for (int x = 0; x < width; x++)
                Console.Write('-');
            Console.WriteLine('+');
        }

        public static void Clear()
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    data[x, y] = ' ';
                }
            }
        }
    }
}