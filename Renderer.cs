namespace Practice
{
    public class Renderer
    {
        Shape all;

        public void Init() {
            CompositeShape masterShape = new CompositeShape();
            masterShape.AddChild(new Rect(50, 16, 40, 10, 'R', '*'));

            CompositeShape rectEllipse = new CompositeShape();
            rectEllipse.AddChild(new HollowRect(0, 0, 20, 14, '~'));
            rectEllipse.AddChild(new Ellipse(0, 0, 20, 14, 'E', '+'));
            rectEllipse.Move(10, 2);
            masterShape.AddChild(rectEllipse);

            Ellipse ellipse = new Ellipse(0, 0, 10, 15, '8', ',');
            ellipse.Move(64, 4);
            masterShape.AddChild(ellipse);

            HollowEllipse hollowEl = new HollowEllipse(0, 0, 16, 6, 'N');
            hollowEl.Move(54, 7);
            masterShape.AddChild(hollowEl);

            masterShape.AddChild(new Dot(3, 10, 'D'));
            masterShape.AddChild(new Dot(40, 7, 'O'));
            masterShape.AddChild(new Dot(77, 12, 'T'));

            all = masterShape;
        }

        public void Draw() {
            Canvas.Clear();

            all.Draw();

            Canvas.Render();
        }
    }
}